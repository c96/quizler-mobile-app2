import Expo from 'expo';
import React from 'react';
import {  Platform, StyleSheet, Text, View } from 'react-native';
import { Constants, Location, Permissions } from 'expo';
const io = require('socket.io-client');

// Local ngrok URL for websockets
const SocketEndpoint = 'https://348f926a.ngrok.io';

export default class App extends React.Component {
  state = {
    isConnected: false,
    data: null,
  };
  componentDidMount() {

    // web socket ping
    const socket = io(SocketEndpoint, {
      transports: ['websocket'],
    });

    socket.on('connect', () => {
      this.setState({ isConnected: true });
    });

    socket.on('ping', data => {
      this.setState(data);
    });

    // Android emulator check
    // Get location
    if (Platform.OS === 'android' && !Constants.isDevice) {
      this.setState({
        errorMessage: 'Error occurred, please try on device!',
      });
    } else {
      this._getLocationAsync();
    }
  }

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    }

    let location = await Location.getCurrentPositionAsync({});
    this.setState({ location });
  };

  render() {

    let locText = 'Waiting..';
    if (this.state.errorMessage) {
      locText = this.state.errorMessage;
    } else if (this.state.location) {
      locText = // JSON.stringify(this.state.location) +
      "\nLatitude: " + this.state.location.coords.longitude 
      + "\nLongitude: " + this.state.location.coords.latitude;
    }

    return (
      <View style={styles.container}>
        <Text>connected: {this.state.isConnected ? 'true' : 'false'}</Text>
        {this.state.data &&
          <Text>
            ping response: {this.state.data}
          </Text>}
        <Text style={styles.paragraph}>{locText}</Text>
      </View>
    );
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    textAlign: 'center',
  },
});