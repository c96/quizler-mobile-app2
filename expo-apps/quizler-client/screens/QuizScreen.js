import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Button,
} from 'react-native';
import { Constants, Location, Permissions, WebBrowser } from 'expo';

//import submitterComponent from 'submitterComponent';

import { MonoText } from '../components/StyledText';

const io = require('socket.io-client');

// Local ngrok URL for websockets

const SocketEndpoint = 'http://e9d8443a.ngrok.io';

export default class QuizScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      isConnected: false,
      data: null,
      qtype: 'Waiting...',
      question: 'Waiting...',
      answer: 'Waiting...',
      correct: 'Waiting...',
      answeredCorrect: false,
      text: 'Enter answer here.'
    };
  }

  validateAnswer(input) {
    this.state.text = input;
    if ( input == 'Http' ){
      this.state.status = 'true'
    }
  }

  componentDidMount() {

    // web socket ping
    const socket = io(SocketEndpoint, {
      transports: ['websocket'],
    });

    socket.on('connect', () => {
      this.setState({ isConnected: true });
    });

    socket.on('ping', data => {
      this.setState(data);
    });

    socket.on('question', (question) => {
      //console.log(question.qtype);
      //console.log(question);
      this.setState({qtype: question.qtype});
      this.setState({question: question.question});
      this.setState({correct: question.correct});
    });

    socket.on('correct', () => {
      this.setState({answeredCorrect: true});
    });
    socket.on('incorrect', (correct) => {
      this.setState({answeredCorrect: false});
      this.setState({text: 'Correct answer was: ' + correct.correct});
    });
  }


  submitAnswer() {

    //const ans = this.state.answer;

    // web socket ping
    const socket = io(SocketEndpoint, {
      transports: ['websocket'],
    });

    socket.on('connect', () => {
      this.setState({ isConnected: true });
    });

    socket.emit('answer', {answer: 'ans'});
  }
  

  render() {
    return (
      <View style={{flex: 1, padding:20}}>
        <View style={{flex: 1, backgroundColor: '#f4f4f4'}} >
          
        </View>
        <View style={{flex: 2, backgroundColor: '#ecf0f5'}} >

        <Text style={styles.paragraph}>{this.state.question}</Text>

        
        </View>
        <View style={{flex: 3, backgroundColor: '#f4f4f4'}} >
          <TextInput
            style={{height: 40}}
            placeholder="Enter short answer"
            onChangeText={(text) => this.validateAnswer({text})}
          />

          <Button title="Submit" onPress={this.submitAnswer}/>
          </View>
        <View style={{flex: 3, backgroundColor: '#ecf0f5'}} >
          <Text style={styles.paragraph}>Question Status: {this.state.status}</Text>
        <Text style={styles.paragraph}>type:{this.state.qtype}</Text>
        <Text style={styles.paragraph}>answered:{this.state.answeredCorrect}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    textAlign: 'center',
  },
});